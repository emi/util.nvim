# util.nvim

Collection of lua functionality to use in neovim lua scripts

I kept recreating these in different parts and plugins, so now I try to bundle them so they can be made available everywhere.