-- uppercase first char of string
local function capitalize(str)
    return str:gsub("^%l", string.upper)
end

-- convert UpperCamelCase or camelCase to snake_case
-- taken from https://codegolf.stackexchange.com/a/177958
local function snake_case(str)
    return str:gsub('%f[^%l]%u', '_%1'):gsub('%f[^%a]%d', '_%1'):gsub('%f[^%d]%a', '_%1'):gsub('(%u)(%u%l)'
        , '%1_%2'):lower()
end

-- convert snake_case to camelCast
local function camel_case(str)
    return str:gsub("_(.)", string.upper)
end

-- convert snake_cast to UpperCamelCast
local function upper_camel_case(str)
    return camel_case(capitalize(str))
end

-- TODO: monkeypatch string table?

return {
    capitalize = capitalize,
    snake_case = snake_case,
    camel_case = camel_case,
    upper_camel_case = upper_camel_case
}
