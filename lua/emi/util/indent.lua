-- Functionality to deal with text indentation in spaces

local Indent = {}

function Indent:new(nr_spaces)
    local obj = {}
    obj.nr_spaces = nr_spaces or 4
    obj.level = 0
    setmetatable(obj, self)
    self.__index = self
    return obj
end

function Indent:increase()
    self.level = self.level + 1
end

function Indent:decrease()
    self.level = self.level - 1
    if self.level < 0 then
        self.level = 0
    end
end

function Indent:get()
    -- returns empty string when level == 0
    return string.rep(' ', self.level * self.nr_spaces)
end

-- get current level and then increase for next call
function Indent:get_inc()
    local indent = self:get()
    self:increase()
    return indent
end

-- decrease level and then get
function Indent:get_dec()
    self:decrease()
    return self:get()
end

return Indent
